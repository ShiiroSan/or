// +or-.cpp�: d�finit le point d'entr�e pour l'application console.
//

#include "stdafx.h"		//Comment this if it's compiled for linux ^_^

#include <iostream>
#include <stdio.h>      /* printf, scanf, puts, NULL */
#include <stdlib.h>     /* srand, rand */
#include <time.h>       /* time */

#ifdef _WIN32
#include <windows.h>
#define sleep(nbr) Sleep(nbr*1000) //directly sleep in seconds. Yeah you're fucked if you wanna use decimal but it's not my prob :D
#elif __linux__
#include <unistd.h>
#endif

using namespace std;

/*HEADER PARTS*/
void menu_human_vs_machine();
void menu_machine_vs_humain();
void human_vs_machine(unsigned int iDifficulty);

void ClearScreen();

int main()
{
	short choixTypeJeu;
	cout << "Choisi un type de jeu : " << endl
		<< "1] Humain contre Machine" << endl
		<< "2] Machine contre Humain" << endl
		//<< "3] Machine contre Machine (si si, je suis s�rieux xD)" << endl
		<< "0] Quitter ce magnifique jeu nul" << endl << endl;
	cin >> choixTypeJeu;
	cout << endl;
	switch (choixTypeJeu)
	{
	case 0:
		cout << "Well... Cya soon! " << endl;
		sleep(1);
		exit(0);
	case 1:
		menu_human_vs_machine();
	case 2:
		menu_machine_vs_humain();
	case 3:

	default:
		main();
	}
}

void menu_human_vs_machine()
{
	ClearScreen();
	unsigned short huvsmaDifficulty;
	cout << "Choisi un niveau de difficult� : " << endl
		<< "1] Facile : entre 0 et 1000" << endl
		<< "2] Moyen : entre 0 et 5000" << endl
		<< "3] Dur : entre 0 et 10 000" << endl
		<< "0] Revenir au menu principal" << endl << endl;
	cin >> huvsmaDifficulty;
	cout << endl << endl << endl << endl;
	switch (huvsmaDifficulty)
	{
	case 0: main();
	case 1: human_vs_machine(1);
	case 2: human_vs_machine(2);
	case 3: human_vs_machine(3);
	default:
		human_vs_machine(4);
	}

}

void human_vs_machine(unsigned int iDifficulty)
{
	double valueOfMachine, valueOfHuman;
	srand(time(NULL));
	if (iDifficulty == 1)
	{
		iDifficulty = 1000;
	}
	else if (iDifficulty == 2)
	{
		iDifficulty = 5000;
	}
	else if (iDifficulty == 3)
	{
		iDifficulty = 10000;
	}
	else
	{
		iDifficulty = 4294967295;
		cout << "Vtff. Puisque tu veux jouer au plus con alors tu devras trouver une valeur entre 0 et "
			<< iDifficulty << "." << endl << "GL." << endl;
	}
	valueOfMachine = rand() % iDifficulty;
	cout << "Tr�s bien! Donc la valeur cach� ce trouve entre 0 et " << iDifficulty << endl;
	int numberOfTry=0;
	do
	{
		cout << "Entre une valeur. C'est ton " << numberOfTry << " essai(s)." << endl;
		cin >> valueOfHuman;
		if (valueOfHuman > iDifficulty)
			cout << "Mais en fait t'es compl�tement con? Pourquoi tu teste des valeurs sup�rieur � celle qui est cens� pouvoir �tre test� ?" << endl;
		else if (valueOfHuman < 0)
			cout << "Mais en fait t'es compl�tement con? Pourquoi tu teste des valeurs inf�rieur � celle qui est cens� pouvoir �tre test� ?" << endl;
		else if (valueOfHuman > valueOfMachine)
			cout << "-" << endl;
		else if (valueOfHuman < valueOfMachine)
			cout << "+" << endl;
		numberOfTry++;
	} while (valueOfHuman != valueOfMachine);
	cout << "Yahay! You found it! C'�tait donc " << valueOfHuman <<
		endl << "Tu as trouv� en " << numberOfTry << " essai(s)." << endl;
	sleep(0.5);
	cout << "Reccomencer ?";
	bool again;
	cin >> again;
	cout << endl << endl;
	if (again)
	{
		menu_human_vs_machine();
	}
	else if(!again)
		main();
}

void menu_machine_vs_humain()
{
	ClearScreen();

}

void machine_vs_human()
{

}

void ClearScreen()
{
#ifdef _WIN32
	HANDLE                     hStdOut;
	CONSOLE_SCREEN_BUFFER_INFO csbi;
	DWORD                      count;
	DWORD                      cellCount;
	COORD                      homeCoords = { 0, 0 };

	hStdOut = GetStdHandle(STD_OUTPUT_HANDLE);
	if (hStdOut == INVALID_HANDLE_VALUE) return;

	/* Get the number of cells in the current buffer */
	if (!GetConsoleScreenBufferInfo(hStdOut, &csbi)) return;
	cellCount = csbi.dwSize.X *csbi.dwSize.Y;

	/* Fill the entire buffer with spaces */
	if (!FillConsoleOutputCharacter(
		hStdOut,
		(TCHAR) ' ',
		cellCount,
		homeCoords,
		&count
	)) return;

	/* Fill the entire buffer with the current colors and attributes */
	if (!FillConsoleOutputAttribute(
		hStdOut,
		csbi.wAttributes,
		cellCount,
		homeCoords,
		&count
	)) return;

	/* Move the cursor home */
	SetConsoleCursorPosition(hStdOut, homeCoords);
#elif __linux__
	if (!cur_term)
	{
		int result;
		setupterm(NULL, STDOUT_FILENO, &result);
		if (result <= 0) return;
	}

	putp(tigetstr("clear"));
#endif
}
